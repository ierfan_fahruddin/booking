<?php

namespace App\Http\Controllers\Admin;

use App\Models\Studios;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\Admin\StudiosRequest;
use Symfony\Component\HttpFoundation\Response;

class StudiosController extends Controller
{
    use MediaUploadingTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('studios_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $studios = Studios::all();

        return view('admin.studios.index', compact('studios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('studios_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.studios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudiosRequest $request)
    {
        abort_if(Gate::denies('studios_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $studio = Studios::create($request->validated());
        
        if ($request->input('photo', true)) {
            $studio->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        return redirect()->route('admin.studios.index')->with([
            'message' => 'successfully created !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Studios $studio)
    {
        abort_if(Gate::denies('studios_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.studios.show', compact('studio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Studios $studio)
    {
        abort_if(Gate::denies('studios_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.studios.edit', compact('studio'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudiosRequest $request,Studios $studio)
    {
        abort_if(Gate::denies('studios_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $studio->update($request->validated());

        if ($request->input('photo', false)) {
            if (!$studio->photo || $request->input('photo') !== $studio->photo->file_name) {
                $studio->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($studio->photo) {
            $studio->photo->delete();
        }

        return redirect()->route('admin.studios.index')->with([
            'message' => 'successfully updated !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Studios $studio)
    {
        abort_if(Gate::denies('studios_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $studio->delete();

        return redirect()->route('admin.studios.index')->with([
            'message' => 'successfully deleted !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Delete all selected Permission at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('studios_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        Studios::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
