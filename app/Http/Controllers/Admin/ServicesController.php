<?php

namespace App\Http\Controllers\Admin;

use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\Admin\ServicesRequest;
use Symfony\Component\HttpFoundation\Response;

class ServicesController extends Controller
{
    use MediaUploadingTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('services_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $services = Services::all();

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('services_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicesRequest $request)
    // public function store(Request $request)
    {
        // dd($request->all());
        abort_if(Gate::denies('services_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $service = Services::create($request->validated() + [
            'jam_paket' => $request->jam_paket,
            'jenis_paket' => $request->jenis_paket,
            'denda' => $request->denda,
        ]);
        if ($request->input('photo', false)) {
            $service->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        return redirect()->route('admin.services.index')->with([
            'message' => 'successfully created !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Services $service)
    {
        abort_if(Gate::denies('services_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $service)
    {
        abort_if(Gate::denies('services_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesRequest $request, Services $service)
    {
        abort_if(Gate::denies('services_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $service->update($request->validated() + [
            'jam_paket' => $request->jam_paket,
            'jenis_paket' => $request->jenis_paket,
            'denda' => $request->denda,
        ]);

        if ($request->input('photo', false)) {
            if (!$service->photo || $request->input('photo') !== $service->photo->file_name) {
                $service->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($service->photo) {
            $service->photo->delete();
        }
        return redirect()->route('admin.services.index')->with([
            'message' => 'successfully updated !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $service)
    {
        abort_if(Gate::denies('services_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $service->delete();

        return redirect()->route('admin.services.index')->with([
            'message' => 'successfully deleted !',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Delete all selected Permission at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('services_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        Services::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
